import Vue from 'vue';
import VueRouter from 'vue-router';
import Dashboard from '../views/pages/Dashboard'
import Management from '../views/pages/users/Management'
import Transactions from '../views/pages/users/Transactions'

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'dashboard',
            component: Dashboard,
        },
        {
            path: '/management',
            name: 'user-management',
            component: Management
        },
        {
            path: '/transactions',
            name: 'user-transactions',
            component: Transactions
        }

    ]
});

export default router;
