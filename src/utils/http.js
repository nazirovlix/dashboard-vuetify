import axios from 'axios'

const http = axios.create({
    baseURL: 'http://google.com',
    headers: {
        language: 'en'
    }
});
export default http;