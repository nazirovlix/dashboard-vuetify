import Vue from 'vue'
import store from './store';
import router from './router';
import http from './utils/http'
import vuetify from './plugins/vuetify';
import App from './views/layouts/MainLayout'

import VueMoment from 'vue-moment';

Vue.config.productionTip = false;
Vue.prototype.$http = http;

Vue.use(VueMoment);


window.admin = new Vue({
    el: '#app',
    vuetify,
    store,
    router,
    render: h => h(App),
});
